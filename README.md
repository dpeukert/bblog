# Plugins

[![Embedded Vimeo Videos](https://getbblog.com/plugins/user/148492.jpg)](https://getbblog.com/plugins/user/148492.js)

[Archived forum thread](http://getbblog.com/forums/148490.html) - [GitLab file](plugins/embedded-vimeo-videos.js)

[![General Enhancements](https://getbblog.com/plugins/user/126690.jpg)](https://getbblog.com/plugins/user/126690.js)

[Archived forum thread](http://getbblog.com/forums/123872.html) - [GitLab file](plugins/general-enhancements.js)

[![Hide Forum](https://getbblog.com/plugins/user/126694.jpg)](https://getbblog.com/plugins/user/126694.js)

[Archived forum thread](http://getbblog.com/forums/80453.html) - [GitLab file](plugins/hide-forum.js)

[![Missions Screen Enhancer](https://getbblog.com/plugins/user/126692.jpg)](https://getbblog.com/plugins/user/126692.js)

[Archived forum thread](http://getbblog.com/forums/111914.html) - [GitLab file](plugins/missions-screen-enhancer.js)

[![Platoon Dropdown](https://getbblog.com/plugins/user/126693.jpg)](https://getbblog.com/plugins/user/126693.js)

[Archived forum thread](http://getbblog.com/forums/107331.html) - [GitLab file](plugins/platoon-dropdown.js)

[![Show by DLC](https://getbblog.com/plugins/user/126691.jpg)](https://getbblog.com/plugins/user/126691.js)

[Archived forum thread](http://getbblog.com/forums/115142.html) - [GitLab file](plugins/show-by-dlc.js)

[![To Top in Loadout](https://getbblog.com/plugins/user/126695.jpg)](https://getbblog.com/plugins/user/126695.js)

[Archived forum thread](http://getbblog.com/forums/80157.html) - [GitLab file](plugins/to-top-in-loadout.js)

[![Add Twitter Feed](https://getbblog.com/plugins/user/138606.jpg)](https://getbblog.com/plugins/user/138606.js)

[Archived forum thread](http://getbblog.com/forums/138602.html) - [GitLab file](plugins/add-twitter-feed.js)

[![Auto Expand Battlescreen](https://getbblog.com/plugins/user/137332.jpg)](https://getbblog.com/plugins/user/137332.js)

[Archived forum thread](http://getbblog.com/forums/137330.html) - [GitLab file](plugins/auto-expand-battlescreen.js)

[![Disable Dropdowns](https://getbblog.com/plugins/user/138648.jpg)](https://getbblog.com/plugins/user/138648.js)

[Archived forum thread](http://getbblog.com/forums/138646.html) - [GitLab file](plugins/disable-dropdowns.js)

[![French Spelling Correction](https://getbblog.com/plugins/user/138664.jpg)](https://getbblog.com/plugins/user/138664.js)

[Archived forum thread](http://getbblog.com/forums/138662.html) - [GitLab file](plugins/french-spelling-correction.js)

[![Move BBLog Button](https://getbblog.com/plugins/user/137542.jpg)](https://getbblog.com/plugins/user/137542.js)

[Archived forum thread](http://getbblog.com/forums/137540.html) - [GitLab file](plugins/move-bblog-button.js)

[![Second Assault Map Names Correction](https://getbblog.com/plugins/user/137519.jpg)](https://getbblog.com/plugins/user/137519.js)

[Archived forum thread](http://getbblog.com/forums/137517.html) - [GitLab file](plugins/second-assault-map-names-correction.js)

# Themes
## Dark Blue Chocolate
[GitLab file](themes/dark-blue-chocolate.css)

## Green Chocolate
[GitLab file](themes/green-chocolate.css)

# License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`).
